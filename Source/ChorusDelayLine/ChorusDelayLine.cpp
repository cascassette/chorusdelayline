#include "ChorusDelayLine.h"
//#include <xmmintrin.h>
//_MM_SET_FLUSH_ZERO_MODE (_MM_FLUSH_ZERO_ON);

ChorusDelayLine::ChorusDelayLine () :
    wgChorusL (MAX_CHORUS_DELAY),
    wgChorusR (MAX_CHORUS_DELAY),
    wgDelayL (MAX_DELAY_DELAY),
    wgDelayR (MAX_DELAY_DELAY)
{
    fFeedbackSampleL = fFeedbackSampleR =
    fCRate = fCDepth = fCAmount =
    fDTime = fDFeedback = fDAmount = 0.f;

    dSampleRate =
    dChorusPhase = dChorusDeltaPhase =
    dDelayTimeInSamples = 0.;

    clippingCounter = 0;

    clipping = false;
}

ChorusDelayLine::~ChorusDelayLine() { }

void ChorusDelayLine::setCRate (float nCRate)
{
    fCRate = nCRate;
    if (dSampleRate != 0)
        dChorusDeltaPhase = (double)fCRate / (double)dSampleRate;
}

void ChorusDelayLine::setCDepth (float nCDepth)
{
    fCDepth = nCDepth;
}

void ChorusDelayLine::setCAmount (float nCAmount)
{
    fCAmount = nCAmount;
}

void ChorusDelayLine::setCPhase (float nCPhase)
{
    fCPhase = nCPhase;
}

void ChorusDelayLine::setDTime (float nDTime)
{
    fDTime = nDTime;
    if (dSampleRate != 0)
        dDelayTimeInSamples = (double)fDTime / 1000. * dSampleRate;
}

void ChorusDelayLine::setDFeedback (float nDFeedback)
{
    fDFeedback = nDFeedback;
}

void ChorusDelayLine::setDAmount (float nDAmount)
{
    fDAmount = nDAmount;
}

void ChorusDelayLine::setSampleRate (double nSampleRate)
{
    dSampleRate = nSampleRate;
    setCRate (fCRate);
    setDTime (fDTime);
}

void ChorusDelayLine::clockProcess (float* LeftSample, float* RightSample)
{
    if (dSampleRate != 0)
    {
        // input
        //CC_SNAP_TO_ZERO (fDFeedback);
        float inL = *LeftSample + fDFeedback * fFeedbackSampleL;
        float inR = *RightSample + fDFeedback * fFeedbackSampleR;
        CC_SNAP_TO_ZERO (inL);
        CC_SNAP_TO_ZERO (inR);

        // check clipping
        clippingCounter++;
        if (clippingCounter > MAX_CLIPPING_COUNTER)
            clipping = false;

        if (inL < -1.f)
        {
            inL = -1.f;
            clipping = true;
            clippingCounter = 0;
        }
        else if (inL > 1.f)
        {
            inL = 1.f;
            clipping = true;
            clippingCounter = 0;
        }

        if (inR < -1.f)
        {
            inR = -1.f;
            clipping = true;
            clippingCounter = 0;
        }
        else if (inR > 1.f)
        {
            inR = 1.f;
            clipping = true;
            clippingCounter = 0;
        }

        // compute chorus lfo phase
        dChorusPhase = fmod (dChorusPhase + dChorusDeltaPhase, 1.);
        double depthL = unary (sin (dChorusPhase * 2. * double_Pi)) * fCDepth + 1.;
        double depthR = unary (sin ((dChorusPhase + fCPhase) * 2. * double_Pi)) * fCDepth + 1.;
        float stage1L = (1 - fCAmount) * inL + (fCAmount) * wgChorusL.feed (inL, depthL);
        float stage1R = (1 - fCAmount) * inR + (fCAmount) * wgChorusR.feed (inR, depthR);

        // compute delay
        float stage2LWet = wgDelayL.feed (stage1L, dDelayTimeInSamples + 1.);
        float stage2RWet = wgDelayR.feed (stage1R, dDelayTimeInSamples + 1.);

        // store feedback sample
        //CC_SNAP_TO_ZERO (fDFeedback);
        fFeedbackSampleL = stage2LWet;
        fFeedbackSampleR = stage2RWet;
        /*fFeedbackSampleL = fDFeedback * stage2LWet;
          fFeedbackSampleR = fDFeedback * stage2RWet;
          CC_SNAP_TO_ZERO (fFeedbackSampleL);
          CC_SNAP_TO_ZERO (fFeedbackSampleR);*/

        // output
        *LeftSample = (1 - fDAmount) * stage1L + (fDAmount) * stage2LWet;
        *RightSample = (1 - fDAmount) * stage1R + (fDAmount) * stage2RWet;
    }
}
