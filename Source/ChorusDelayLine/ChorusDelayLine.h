#include "cwaveguide.h"
#define CC_SNAP_TO_ZERO(n)		if (! (n < -1.0e-8 || n > 1.0e-8)) n = 0;

const int MAX_CHORUS_DELAY = 2400;  // ~ 50 msec
const int MAX_DELAY_DELAY = 48000;  // ~ 1 second
const int MAX_CLIPPING_COUNTER = 24000;

class ChorusDelayLine
{
public:
                       ChorusDelayLine();
                      ~ChorusDelayLine();

    // getters
    float              getCRate()     { return fCRate; }
    float              getCDepth()    { return fCDepth; }
    float              getCAmount()   { return fCAmount; }
    float              getCPhase()    { return fCPhase; }
    float              getDTime()     { return fDTime; }
    float              getDFeedback() { return fDFeedback; }
    float              getDAmount()   { return fDAmount; }
    // setters
    void               setCRate (float nCRate);
    void               setCDepth (float nCDepth);
    void               setCAmount (float nCAmount);
    void               setCPhase (float nCPhase);
    void               setDTime (float nDTime);
    void               setDFeedback (float nDFeedback);
    void               setDAmount (float nDAmount);

    void               setSampleRate (double nSampleRate);

    bool               getClipping () { return clipping; }

    void               clockProcess (float* LeftSample, float* RightSample);

private:
    inline double      unary (double val) { val = val * .5 + .5; CC_SNAP_TO_ZERO (val); return val; }

    float              fFeedbackSampleL;
    float              fFeedbackSampleR;
    double             dSampleRate;

    float              fCRate;
    float              fCDepth;
    float              fCAmount;
    float              fCPhase;
    double             dChorusPhase;
    double             dChorusDeltaPhase;
    cwaveguide         wgChorusL;
    cwaveguide         wgChorusR;

    float              fDTime;
    float              fDFeedback;
    float              fDAmount;
    double             dDelayTimeInSamples;
    cwaveguide         wgDelayL;
    cwaveguide         wgDelayR;

    bool               clipping;
    int                clippingCounter;
};
