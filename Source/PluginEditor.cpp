#include "PluginEditor.h"

ChorusDelayLineAudioProcessorEditor::ChorusDelayLineAudioProcessorEditor (ChorusDelayLineAudioProcessor* ownerFilter)
    : AudioProcessorEditor(ownerFilter)
{
    addAndMakeVisible (sldCRate = new Slider ("new slider"));
    sldCRate->setRange (0, 1, 0);
    sldCRate->setSliderStyle (Slider::LinearHorizontal);
    sldCRate->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sldCRate->addListener (this);
    sldCRate->setSkewFactor (0.5);

    addAndMakeVisible (sldCDepth = new Slider ("new slider"));
    sldCDepth->setRange (0, 1, 0);
    sldCDepth->setSliderStyle (Slider::LinearHorizontal);
    sldCDepth->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sldCDepth->addListener (this);

    addAndMakeVisible (sldCAmount = new Slider ("new slider"));
    sldCAmount->setRange (0, 1, 0);
    sldCAmount->setSliderStyle (Slider::LinearHorizontal);
    sldCAmount->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sldCAmount->addListener (this);

    addAndMakeVisible (sldDTime = new Slider ("new slider"));
    sldDTime->setRange (0, 1, 0);
    sldDTime->setSliderStyle (Slider::LinearHorizontal);
    sldDTime->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sldDTime->addListener (this);
    sldDTime->setSkewFactor (0.5);

    addAndMakeVisible (sldDFeedback = new Slider ("new slider"));
    sldDFeedback->setRange (0, 1, 0);
    sldDFeedback->setSliderStyle (Slider::LinearHorizontal);
    sldDFeedback->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sldDFeedback->addListener (this);

    addAndMakeVisible (sldDAmount = new Slider ("new slider"));
    sldDAmount->setRange (0, 1, 0);
    sldDAmount->setSliderStyle (Slider::LinearHorizontal);
    sldDAmount->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sldDAmount->addListener (this);

    addAndMakeVisible (lblCRate = new Label ("new label",
                                             TRANS("C. Rate")));
    lblCRate->setFont (Font (15.00f, Font::plain));
    lblCRate->setJustificationType (Justification::centredLeft);
    lblCRate->setEditable (false, false, false);
    lblCRate->setColour (Label::textColourId, Colours::white);
    lblCRate->setColour (TextEditor::textColourId, Colours::black);
    lblCRate->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (lblCDepth = new Label ("new label",
                                              TRANS("C. Depth")));
    lblCDepth->setFont (Font (15.00f, Font::plain));
    lblCDepth->setJustificationType (Justification::centredLeft);
    lblCDepth->setEditable (false, false, false);
    lblCDepth->setColour (Label::textColourId, Colours::white);
    lblCDepth->setColour (TextEditor::textColourId, Colours::black);
    lblCDepth->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (lblCAmount = new Label ("new label",
                                               TRANS("C. Amount")));
    lblCAmount->setFont (Font (15.00f, Font::plain));
    lblCAmount->setJustificationType (Justification::centredLeft);
    lblCAmount->setEditable (false, false, false);
    lblCAmount->setColour (Label::textColourId, Colours::white);
    lblCAmount->setColour (TextEditor::textColourId, Colours::black);
    lblCAmount->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (lblDTime = new Label ("new label",
                                             TRANS("D. Time")));
    lblDTime->setFont (Font (15.00f, Font::plain));
    lblDTime->setJustificationType (Justification::centredLeft);
    lblDTime->setEditable (false, false, false);
    lblDTime->setColour (Label::textColourId, Colours::white);
    lblDTime->setColour (TextEditor::textColourId, Colours::black);
    lblDTime->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (lblDFeedback = new Label ("new label",
                                                 TRANS("D. Feedback")));
    lblDFeedback->setFont (Font (15.00f, Font::plain));
    lblDFeedback->setJustificationType (Justification::centredLeft);
    lblDFeedback->setEditable (false, false, false);
    lblDFeedback->setColour (Label::textColourId, Colours::white);
    lblDFeedback->setColour (TextEditor::textColourId, Colours::black);
    lblDFeedback->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (lblDAmount = new Label ("new label",
                                               TRANS("D. Amount")));
    lblDAmount->setFont (Font (15.00f, Font::plain));
    lblDAmount->setJustificationType (Justification::centredLeft);
    lblDAmount->setEditable (false, false, false);
    lblDAmount->setColour (Label::textColourId, Colours::white);
    lblDAmount->setColour (TextEditor::textColourId, Colours::black);
    lblDAmount->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (shwCRate = new Label ("new label",
                                             TRANS("0")));
    shwCRate->setFont (Font (15.00f, Font::plain));
    shwCRate->setJustificationType (Justification::centredLeft);
    shwCRate->setEditable (false, false, false);
    shwCRate->setColour (Label::textColourId, Colours::white);
    shwCRate->setColour (TextEditor::textColourId, Colours::black);
    shwCRate->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (shwCDepth = new Label ("new label",
                                              TRANS("0")));
    shwCDepth->setFont (Font (15.00f, Font::plain));
    shwCDepth->setJustificationType (Justification::centredLeft);
    shwCDepth->setEditable (false, false, false);
    shwCDepth->setColour (Label::textColourId, Colours::white);
    shwCDepth->setColour (TextEditor::textColourId, Colours::black);
    shwCDepth->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (shwCAmount = new Label ("new label",
                                               TRANS("0")));
    shwCAmount->setFont (Font (15.00f, Font::plain));
    shwCAmount->setJustificationType (Justification::centredLeft);
    shwCAmount->setEditable (false, false, false);
    shwCAmount->setColour (Label::textColourId, Colours::white);
    shwCAmount->setColour (TextEditor::textColourId, Colours::black);
    shwCAmount->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (shwDTime = new Label ("new label",
                                             TRANS("0")));
    shwDTime->setFont (Font (15.00f, Font::plain));
    shwDTime->setJustificationType (Justification::centredLeft);
    shwDTime->setEditable (false, false, false);
    shwDTime->setColour (Label::textColourId, Colours::white);
    shwDTime->setColour (TextEditor::textColourId, Colours::black);
    shwDTime->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (shwDFeedback = new Label ("new label",
                                                 TRANS("0")));
    shwDFeedback->setFont (Font (15.00f, Font::plain));
    shwDFeedback->setJustificationType (Justification::centredLeft);
    shwDFeedback->setEditable (false, false, false);
    shwDFeedback->setColour (Label::textColourId, Colours::white);
    shwDFeedback->setColour (TextEditor::textColourId, Colours::black);
    shwDFeedback->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (shwDAmount = new Label ("new label",
                                               TRANS("0")));
    shwDAmount->setFont (Font (15.00f, Font::plain));
    shwDAmount->setJustificationType (Justification::centredLeft);
    shwDAmount->setEditable (false, false, false);
    shwDAmount->setColour (Label::textColourId, Colours::white);
    shwDAmount->setColour (TextEditor::textColourId, Colours::black);
    shwDAmount->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (sldCPhase = new Slider ("new slider"));
    sldCPhase->setRange (0, 1, 0);
    sldCPhase->setSliderStyle (Slider::LinearHorizontal);
    sldCPhase->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sldCPhase->addListener (this);

    addAndMakeVisible (lblCPhase = new Label ("new label",
                                              TRANS("C. Phase")));
    lblCPhase->setFont (Font (15.00f, Font::plain));
    lblCPhase->setJustificationType (Justification::centredLeft);
    lblCPhase->setEditable (false, false, false);
    lblCPhase->setColour (Label::textColourId, Colours::white);
    lblCPhase->setColour (TextEditor::textColourId, Colours::black);
    lblCPhase->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (shwCPhase = new Label ("new label",
                                              TRANS("0")));
    shwCPhase->setFont (Font (15.00f, Font::plain));
    shwCPhase->setJustificationType (Justification::centredLeft);
    shwCPhase->setEditable (false, false, false);
    shwCPhase->setColour (Label::textColourId, Colours::white);
    shwCPhase->setColour (TextEditor::textColourId, Colours::black);
    shwCPhase->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    setSize (350, 240);

    startTimer (200);
    getProcessor()->requestUIUpdate();
    clipping = false;
}

ChorusDelayLineAudioProcessorEditor::~ChorusDelayLineAudioProcessorEditor()
{
    sldCRate = nullptr;
    sldCDepth = nullptr;
    sldCAmount = nullptr;
    sldCPhase = nullptr;
    sldDTime = nullptr;
    sldDFeedback = nullptr;
    sldDAmount = nullptr;
    lblCRate = nullptr;
    lblCDepth = nullptr;
    lblCAmount = nullptr;
    lblCPhase = nullptr;
    lblDTime = nullptr;
    lblDFeedback = nullptr;
    lblDAmount = nullptr;
    shwCRate = nullptr;
    shwCDepth = nullptr;
    shwCAmount = nullptr;
    shwCPhase = nullptr;
    shwDTime = nullptr;
    shwDFeedback = nullptr;
    shwDAmount = nullptr;
}

void ChorusDelayLineAudioProcessorEditor::paint (Graphics& g)
{
    g.fillAll (Colours::black);
    if (clipping)
    {
	g.setColour (Colour (255, 0, 0));
	g.fillRect (0, 0, 10, 200);
    }
}

void ChorusDelayLineAudioProcessorEditor::resized()
{
    sldCRate->setBounds (112, 8, 160, 24);
    sldCDepth->setBounds (112, 40, 160, 24);
    sldCAmount->setBounds (112, 72, 160, 24);
    sldDTime->setBounds (112, 136, 160, 24);
    sldDFeedback->setBounds (112, 168, 160, 24);
    sldDAmount->setBounds (112, 200, 160, 24);
    lblCRate->setBounds (8, 8, 96, 24);
    lblCDepth->setBounds (8, 40, 96, 24);
    lblCAmount->setBounds (8, 72, 96, 24);
    lblDTime->setBounds (8, 136, 96, 24);
    lblDFeedback->setBounds (8, 168, 96, 24);
    lblDAmount->setBounds (8, 200, 96, 24);
    shwCRate->setBounds (280, 8, 64, 24);
    shwCDepth->setBounds (280, 40, 64, 24);
    shwCAmount->setBounds (280, 72, 64, 24);
    shwDTime->setBounds (280, 136, 64, 24);
    shwDFeedback->setBounds (280, 168, 64, 24);
    shwDAmount->setBounds (280, 200, 64, 24);
    sldCPhase->setBounds (112, 103, 160, 24);
    lblCPhase->setBounds (8, 103, 96, 24);
    shwCPhase->setBounds (280, 103, 64, 24);
}

void ChorusDelayLineAudioProcessorEditor::sliderValueChanged (Slider* sliderThatWasMoved)
{
    ChorusDelayLineAudioProcessor* ourProcessor = getProcessor();

    if (sliderThatWasMoved == sldCRate)
    {
        ourProcessor->setParameter (ChorusDelayLineAudioProcessor::CRate, (float)sliderThatWasMoved->getValue());
    }
    else if (sliderThatWasMoved == sldCDepth)
    {
        ourProcessor->setParameter (ChorusDelayLineAudioProcessor::CDepth, (float)sliderThatWasMoved->getValue());
    }
    else if (sliderThatWasMoved == sldCAmount)
    {
        ourProcessor->setParameter (ChorusDelayLineAudioProcessor::CAmount, (float)sliderThatWasMoved->getValue());
    }
    else if (sliderThatWasMoved == sldCPhase)
    {
        ourProcessor->setParameter (ChorusDelayLineAudioProcessor::CPhase, (float)sliderThatWasMoved->getValue());
    }
    else if (sliderThatWasMoved == sldDTime)
    {
        ourProcessor->setParameter (ChorusDelayLineAudioProcessor::DTime, (float)sliderThatWasMoved->getValue());
    }
    else if (sliderThatWasMoved == sldDFeedback)
    {
        ourProcessor->setParameter (ChorusDelayLineAudioProcessor::DFeedback, (float)sliderThatWasMoved->getValue());
    }
    else if (sliderThatWasMoved == sldDAmount)
    {
        ourProcessor->setParameter (ChorusDelayLineAudioProcessor::DAmount, (float)sliderThatWasMoved->getValue());
    }
}

void ChorusDelayLineAudioProcessorEditor::timerCallback()
{
    ChorusDelayLineAudioProcessor* ourProcessor = getProcessor();
    clipping = ourProcessor->getClipping();
    if (ourProcessor->needsUIUpdate())
    {
	sldCRate->setValue (ourProcessor->getParameter (ChorusDelayLineAudioProcessor::CRate), dontSendNotification);
	sldCDepth->setValue (ourProcessor->getParameter (ChorusDelayLineAudioProcessor::CDepth), dontSendNotification);
	sldCAmount->setValue (ourProcessor->getParameter (ChorusDelayLineAudioProcessor::CAmount), dontSendNotification);
	sldCPhase->setValue (ourProcessor->getParameter (ChorusDelayLineAudioProcessor::CPhase), dontSendNotification);
	sldDTime->setValue (ourProcessor->getParameter (ChorusDelayLineAudioProcessor::DTime), dontSendNotification);
	sldDFeedback->setValue (ourProcessor->getParameter (ChorusDelayLineAudioProcessor::DFeedback), dontSendNotification);
	sldDAmount->setValue (ourProcessor->getParameter (ChorusDelayLineAudioProcessor::DAmount), dontSendNotification);
	shwCRate->setText (ourProcessor->getParameterText (ChorusDelayLineAudioProcessor::CRate), dontSendNotification);
	shwCDepth->setText (ourProcessor->getParameterText (ChorusDelayLineAudioProcessor::CDepth), dontSendNotification);
	shwCAmount->setText (ourProcessor->getParameterText (ChorusDelayLineAudioProcessor::CAmount), dontSendNotification);
	shwCPhase->setText (ourProcessor->getParameterText (ChorusDelayLineAudioProcessor::CPhase), dontSendNotification);
	shwDTime->setText (ourProcessor->getParameterText (ChorusDelayLineAudioProcessor::DTime), dontSendNotification);
	shwDFeedback->setText (ourProcessor->getParameterText (ChorusDelayLineAudioProcessor::DFeedback), dontSendNotification);
	shwDAmount->setText (ourProcessor->getParameterText (ChorusDelayLineAudioProcessor::DAmount), dontSendNotification);
    }
}


//==============================================================================
#if 0
/*  -- Introjucer information section --

    This is where the Introjucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="ChorusDelayLineAudioProcessorEditor"
                 componentName="" parentClasses="public AudioProcessorEditor, public Timer"
                 constructorParams="ChorusDelayLineAudioProcessor* ownerFilter"
                 variableInitialisers="AudioProcessorEditor(ownerFilter)" snapPixels="8"
                 snapActive="1" snapShown="1" overlayOpacity="0.330" fixedSize="1"
                 initialWidth="350" initialHeight="240">
  <BACKGROUND backgroundColour="ff000000"/>
  <SLIDER name="new slider" id="222e713d375146d" memberName="sldCRate"
          virtualName="" explicitFocusOrder="0" pos="112 8 160 24" min="0"
          max="1" int="0" style="LinearHorizontal" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.5"/>
  <SLIDER name="new slider" id="9f7269b2c3e10aef" memberName="sldCDepth"
          virtualName="" explicitFocusOrder="0" pos="112 40 160 24" min="0"
          max="1" int="0" style="LinearHorizontal" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1"/>
  <SLIDER name="new slider" id="3dea9b26154f9844" memberName="sldCAmount"
          virtualName="" explicitFocusOrder="0" pos="112 72 160 24" min="0"
          max="1" int="0" style="LinearHorizontal" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1"/>
  <SLIDER name="new slider" id="7b37530e1d3dab66" memberName="sldDTime"
          virtualName="" explicitFocusOrder="0" pos="112 136 160 24" min="0"
          max="1" int="0" style="LinearHorizontal" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.5"/>
  <SLIDER name="new slider" id="4bf288751bfeb66d" memberName="sldDFeedback"
          virtualName="" explicitFocusOrder="0" pos="112 168 160 24" min="0"
          max="1" int="0" style="LinearHorizontal" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1"/>
  <SLIDER name="new slider" id="eb24b0dfb2ed0e29" memberName="sldDAmount"
          virtualName="" explicitFocusOrder="0" pos="112 200 160 24" min="0"
          max="1" int="0" style="LinearHorizontal" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1"/>
  <LABEL name="new label" id="aeb25cbcf0baa1d" memberName="lblCRate" virtualName=""
         explicitFocusOrder="0" pos="8 8 96 24" textCol="ffffffff" edTextCol="ff000000"
         edBkgCol="0" labelText="C. Rate" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="Default font" fontsize="15"
         bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="6fbc04fd34a60f6b" memberName="lblCDepth"
         virtualName="" explicitFocusOrder="0" pos="8 40 96 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="C. Depth" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="7ea08ab72ed92be1" memberName="lblCAmount"
         virtualName="" explicitFocusOrder="0" pos="8 72 96 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="C. Amount" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="a8968746206fa313" memberName="lblDTime"
         virtualName="" explicitFocusOrder="0" pos="8 136 96 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="D. Time" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="130a9684f5efb399" memberName="lblDFeedback"
         virtualName="" explicitFocusOrder="0" pos="8 168 96 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="D. Feedback" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="bbfc621a8460973b" memberName="lblDAmount"
         virtualName="" explicitFocusOrder="0" pos="8 200 96 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="D. Amount" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="689b9f334f159b44" memberName="shwCRate"
         virtualName="" explicitFocusOrder="0" pos="280 8 64 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="de76fee0aea526b2" memberName="shwCDepth"
         virtualName="" explicitFocusOrder="0" pos="280 40 64 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="c88f2097cdeaed3e" memberName="shwCAmount"
         virtualName="" explicitFocusOrder="0" pos="280 72 64 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="38b0627f00b45b7b" memberName="shwDTime"
         virtualName="" explicitFocusOrder="0" pos="280 136 64 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="4535fbcff7e7d7d0" memberName="shwDFeedback"
         virtualName="" explicitFocusOrder="0" pos="280 168 64 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="223199e50ddf414c" memberName="shwDAmount"
         virtualName="" explicitFocusOrder="0" pos="280 200 64 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <SLIDER name="new slider" id="8c0f0db1128e879e" memberName="sldCPhase"
          virtualName="" explicitFocusOrder="0" pos="112 103 160 24" min="0"
          max="1" int="0" style="LinearHorizontal" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1"/>
  <LABEL name="new label" id="4d9febf7fee46b6e" memberName="lblCPhase"
         virtualName="" explicitFocusOrder="0" pos="8 103 96 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="C. Phase" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="4e686d4971e5437e" memberName="shwCPhase"
         virtualName="" explicitFocusOrder="0" pos="280 103 64 24" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif
