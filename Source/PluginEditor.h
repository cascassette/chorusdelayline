#pragma once

#include "JuceHeader.h"
#include "PluginProcessor.h"

class ChorusDelayLineAudioProcessorEditor  : public AudioProcessorEditor,
                                             public Timer,
                                             public SliderListener
{
public:
    ChorusDelayLineAudioProcessorEditor (ChorusDelayLineAudioProcessor* ownerFilter);
    ~ChorusDelayLineAudioProcessorEditor();

    void timerCallback();
    ChorusDelayLineAudioProcessor* getProcessor() const
    {
        return static_cast <ChorusDelayLineAudioProcessor*>(getAudioProcessor());
    }

    void paint (Graphics& g);
    void resized();
    void sliderValueChanged (Slider* sliderThatWasMoved);

private:
    bool clipping;

    ScopedPointer<Slider> sldCRate;
    ScopedPointer<Slider> sldCDepth;
    ScopedPointer<Slider> sldCAmount;
    ScopedPointer<Slider> sldDTime;
    ScopedPointer<Slider> sldDFeedback;
    ScopedPointer<Slider> sldDAmount;
    ScopedPointer<Label> lblCRate;
    ScopedPointer<Label> lblCDepth;
    ScopedPointer<Label> lblCAmount;
    ScopedPointer<Label> lblDTime;
    ScopedPointer<Label> lblDFeedback;
    ScopedPointer<Label> lblDAmount;
    ScopedPointer<Label> shwCRate;
    ScopedPointer<Label> shwCDepth;
    ScopedPointer<Label> shwCAmount;
    ScopedPointer<Label> shwDTime;
    ScopedPointer<Label> shwDFeedback;
    ScopedPointer<Label> shwDAmount;
    ScopedPointer<Slider> sldCPhase;
    ScopedPointer<Label> lblCPhase;
    ScopedPointer<Label> shwCPhase;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ChorusDelayLineAudioProcessorEditor)
};
