#include "PluginProcessor.h"
#include "PluginEditor.h"

ChorusDelayLineAudioProcessor::ChorusDelayLineAudioProcessor()
{
    for (int i = 0; i < totalNumParam; i++)
        Params[i] = 0.f;
}

ChorusDelayLineAudioProcessor::~ChorusDelayLineAudioProcessor()
{
}

const String ChorusDelayLineAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

void ChorusDelayLineAudioProcessor::setParameter (int index, float newValue)
{
    if (index >= 0 && index < totalNumParam)
    {
	Params[index] = newValue;
	switch (index)
	{
	case CRate:	thatChorusDelayLine.setCRate (newValue * (maxCRate - minCRate) + minCRate); break;
	case CDepth:	thatChorusDelayLine.setCDepth (newValue * (maxCDepth - minCDepth) + minCDepth); break;
	case CAmount:	thatChorusDelayLine.setCAmount (newValue); break;
	case CPhase:	thatChorusDelayLine.setCPhase (newValue * .5f);
	case DTime:	thatChorusDelayLine.setDTime (newValue * (maxDTime - minDTime) + minDTime); break;
	case DFeedback:	thatChorusDelayLine.setDFeedback (newValue); break;
	case DAmount:	thatChorusDelayLine.setDAmount (newValue); break;
	}
	UIUpdateFlag = true;
    }
}

const String ChorusDelayLineAudioProcessor::getParameterName (int index)
{
    if (index >= 0 && index < totalNumParam)
    {
	switch (index)
	{
	case CRate:	return "C. Rate";
	case CDepth:	return "C. Depth";
	case CAmount:	return "C. Amount";
	case CPhase:	return "C. Phase";
	case DTime:	return "D. Time";
	case DFeedback:	return "D. Feedback";
	case DAmount:	return "D. Amount";
	default:	return "";
	}
    }
    else return "";
}

const String ChorusDelayLineAudioProcessor::getParameterText (int index)
{
    if (index >= 0 && index < totalNumParam)
    {
	switch (index)
	{
	case CRate:	return String (Params[index] * (maxCRate - minCRate) + minCRate, 3) + " Hz";
	case CDepth:	return String ((Params[index] * (maxCDepth - minCDepth) + minCDepth) * 1000. / getSampleRate (), 1) + " ms";
	case CAmount:	return String (Params[index] * 100.f, 1) + "%";
	case CPhase:	return String (Params[index] * 180.f, 2) + "�";
	case DTime:	return String (Params[index] * (maxDTime - minDTime) + minDTime, 1) + " ms";
	case DFeedback:	return String (Params[index] * 100.f, 1) + "%";
	case DAmount:	return String (Params[index] * 100.f, 1) + "%";
	default:	return "";
	}
    }
    else return "";
}

const String ChorusDelayLineAudioProcessor::getInputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

const String ChorusDelayLineAudioProcessor::getOutputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

bool ChorusDelayLineAudioProcessor::isInputChannelStereoPair (int /*index*/) const
{
    return true;
}

bool ChorusDelayLineAudioProcessor::isOutputChannelStereoPair (int /*index*/) const
{
    return true;
}

bool ChorusDelayLineAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool ChorusDelayLineAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool ChorusDelayLineAudioProcessor::silenceInProducesSilenceOut() const
{
    return false;
}

double ChorusDelayLineAudioProcessor::getTailLengthSeconds() const
{
    return 20.0;
}

int ChorusDelayLineAudioProcessor::getNumPrograms()
{
    return 0;
}

int ChorusDelayLineAudioProcessor::getCurrentProgram()
{
    return 0;
}

void ChorusDelayLineAudioProcessor::setCurrentProgram (int /*index*/)
{
}

const String ChorusDelayLineAudioProcessor::getProgramName (int /*index*/)
{
    return String::empty;
}

void ChorusDelayLineAudioProcessor::changeProgramName (int /*index*/, const String& /*newName*/)
{
}

void ChorusDelayLineAudioProcessor::prepareToPlay (double sampleRate, int /*samplesPerBlock*/)
{
    thatChorusDelayLine.setSampleRate (sampleRate);
}

void ChorusDelayLineAudioProcessor::releaseResources()
{
}

void ChorusDelayLineAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& /*midiMessages*/)
{
    if (getNumOutputChannels () == 2)
    {
        float smpL = 0, smpR = 0;
        for (int smp = 0; smp < buffer.getNumSamples (); smp++)
        {
                smpL = buffer.getSample (0, smp);
                smpR = buffer.getSample (1, smp);
                thatChorusDelayLine.clockProcess (&smpL, &smpR);
                buffer.setSample (0, smp, smpL);
                buffer.setSample (1, smp, smpR);
        }
    }
    // not supported yet
    /*
    else
    {
        float* samples = buffer.getSampleData (0);
    }*/
}

bool ChorusDelayLineAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* ChorusDelayLineAudioProcessor::createEditor()
{
    return new ChorusDelayLineAudioProcessorEditor (this);
}

const String ChorusDelayLineAudioProcessor::getParameterNameXml (int index)
{
    if (index >= 0 && index < totalNumParam)
    {
	switch (index)
	{
	case CRate:	return "CRate";
	case CDepth:	return "CDepth";
	case CAmount:	return "CAmount";
	case CPhase:	return "CPhase";
	case DTime:	return "DTime";
	case DFeedback:	return "DFeedback";
	case DAmount:	return "DAmount";
	default:	return "";
	}
    }
    else return "";
}

void ChorusDelayLineAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    XmlElement xml ("CDLPLUGINSETTINGS");
    for (int p = 0; p < totalNumParam; p++)
	xml.setAttribute (getParameterNameXml (p), Params[p]);
    return copyXmlToBinary (xml, destData);
}

void ChorusDelayLineAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    ScopedPointer<XmlElement> xml (getXmlFromBinary (data, sizeInBytes));
    if (xml != nullptr)
	if (xml->hasTagName ("CDLPLUGINSETTINGS"))
            for (int p = 0; p < totalNumParam; p++)
                if (xml->hasAttribute (getParameterNameXml (p)))
                    setParameterNotifyingHost (p, (float)xml->getDoubleAttribute (getParameterNameXml (p)));
}

AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new ChorusDelayLineAudioProcessor();
}
