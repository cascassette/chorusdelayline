#ifndef PLUGINPROCESSOR_H_INCLUDED
#define PLUGINPROCESSOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "ChorusDelayLine/ChorusDelayLine.h"

class ChorusDelayLineAudioProcessor  : public AudioProcessor
{
public:
                            ChorusDelayLineAudioProcessor();
                           ~ChorusDelayLineAudioProcessor();

    void                    prepareToPlay (double sampleRate, int samplesPerBlock);
    void                    releaseResources();

    void                    processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages);

    AudioProcessorEditor*   createEditor();
    bool                    hasEditor() const;

    const String            getName() const;

    int                     getNumParameters() { return totalNumParam; }

    float                   getParameter (int index) { return Params[index]; }
    void                    setParameter (int index, float newValue);

    const String            getParameterName (int index);
    const String            getParameterText (int index);

    const String            getInputChannelName (int channelIndex) const;
    const String            getOutputChannelName (int channelIndex) const;
    bool                    isInputChannelStereoPair (int index) const;
    bool                    isOutputChannelStereoPair (int index) const;

    bool                    acceptsMidi() const;
    bool                    producesMidi() const;
    bool                    silenceInProducesSilenceOut() const;
    double                  getTailLengthSeconds() const;

    int                     getNumPrograms();
    int                     getCurrentProgram();
    void                    setCurrentProgram (int index);
    const String            getProgramName (int index);
    void                    changeProgramName (int index, const String& newName);

    void                    getStateInformation (MemoryBlock& destData);
    void                    setStateInformation (const void* data, int sizeInBytes);
    
    enum Parameters
    {
        CRate = 0,
        CDepth,
        CAmount,
        CPhase,
        
        DTime,
        DFeedback,
        DAmount,
        
        totalNumParam
    };

    bool                    needsUIUpdate() { return UIUpdateFlag; }
    void                    requestUIUpdate() { UIUpdateFlag = true; }
    void                    clearUIUpdate() { UIUpdateFlag = false; }

    bool                    getClipping() { return thatChorusDelayLine.getClipping(); }

private:
    bool                    UIUpdateFlag;

    //const float fPRange[totalNumParam] = { 5.f, 2400.f, 1.f, 1000.f, 1.f, 1.f };
    const float minCRate = .01f;
    const float maxCRate = 2.f;
    const float minCDepth = 8.f;
    const float maxCDepth = 400.f;
    const float minDTime = 2.f;
    const float maxDTime = 800.f;

    /*const float rangeCRate = 2.f;
    const float rangeCDepth = 400.f;
    const float rangeDTime = 800.f;*/

    float Params[totalNumParam];
    ChorusDelayLine thatChorusDelayLine;

    const String getParameterNameXml (int index);

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ChorusDelayLineAudioProcessor)
};

#endif  // PLUGINPROCESSOR_H_INCLUDED
